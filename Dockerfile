#Imagen base
FROM node:latest

#Directorio de la app en el contenedor
WORKDIR /app

#Copiado de archivos
ADD . /app

#puede fallar hay que poner una #(comentario)
#Dependencias
RUN npm install

#Puerto que expongo
EXPOSE 3000

#Comandos  de ejecución de la aplicación
CMD ["npm", "start"]
